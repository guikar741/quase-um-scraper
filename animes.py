"""."""
from io import StringIO
from json import dump, loads
from operator import itemgetter
from re import sub
from unicodedata import combining, normalize

from bs4 import BeautifulSoup as bs
from requests import RequestException, post

from constants import *
import uuid


def req_link_post(u, info):
    """."""
    try:
        resul = post(u, data=info)
    except (Exception, RequestException):
        resul = req_link_post(u, info)
    return resul


def pagina_animes():
    """."""
    link = f"{ANIMES}inc/paginator.inc.php"
    total = 99
    atual = 1
    ani = []
    lista_animes = []
    while atual <= total:
        data = {'type_url': "lista",
                'page': atual,
                'limit': 1000,
                'total_page': total,
                'type': 'lista',
                'filters': '{"filter_data":"filter_letter=0&filter_type_content=100'
                           '&filter_genre_model=0&filter_order=0&filter_rank=0'
                           '&filter_status=0&filter_idade=&filter_dub=0'
                           '&filter_size_start=0&filter_size_final=0&filter_date=0'
                           '&filter_viewed=0",'
                           '"filter_genre_add":[],"filter_genre_del":[]}'}
        e = req_link_post(link, data)
        body = loads(e.content)
        total = body['total_page']
        atual += 1
        for I in range(len(body['body'])):
            novo_ani = dict()
            b = bs(body['body'][I], 'html.parser')
            a = b.find('h1', 'grid_title').find('a')
            novo_ani['nome'] = a.text
            novo_ani['link'] = (a.get('href') if ('http' or 'https') in a.get('href') else
                                "https:" + a.get('href'))
            novo_ani['imagem'] = b.find('img').get('src')
            novo_ani['uuid'] = str(
                uuid.uuid5(
                    uuid.NAMESPACE_DNS, novo_ani['imagem'] + novo_ani['nome'] + novo_ani['link'])
            )
            lista_animes.append(novo_ani)
    for I in sorted(lista_animes, key=itemgetter('nome')):
        ani.append(I)
    io = StringIO()
    dump(ani, io)
    json_s = io.getvalue()
    arq = open(path_local + "PaginasAnimes.json", 'w')
    arq.write(json_s)
    arq.close()


def sanitizestring(palavra):
    """."""
    # Unicode normalize transforma um caracter em seu equivalente em latin.
    nfkd = normalize('NFKD', palavra)
    palavrasemacento = u"".join([c for c in nfkd if not combining(c)])
    # Usa expressão regular para retornar a palavra apenas com números, letras e espaço
    return sub('[^a-zA-Z0-9 ]', '', palavrasemacento)


path_local = 'dados/'
pagina_animes()
