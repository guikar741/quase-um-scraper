"""."""
from requests import post, get, delete
from datetime import datetime
from google_token import access_token

keys = get("https://leitor-mangas-flutter.firebaseio.com/dados.json").json()
if keys and len(keys.keys()) > 0:
    key = [*keys]
    for kk in key:
        nk = [*keys[kk].keys()]
        for i in nk:
            print(
                "DELETE",
                delete(
                    f"https://leitor-mangas-flutter.firebaseio.com/dados/{kk}/{i}.json",
                    headers={"Authorization": f"Bearer {access_token}"}
                ).url
            )

print('POST', post("https://leitor-mangas-flutter.firebaseio.com/dados/mangas.json",
                   data=open('dados/PaginasMangas.json').read(),
                   headers={"Authorization": f"Bearer {access_token}"}).url,)
print("POST", post("https://leitor-mangas-flutter.firebaseio.com/dados/animes.json",
                   data=open('dados/PaginasAnimes.json').read(),
                   headers={"Authorization": f"Bearer {access_token}"}).url)
print("POST", post("https://leitor-mangas-flutter.firebaseio.com/dados/hqs.json",
                   data=open('dados/PaginasHQS.json').read(),
                   headers={"Authorization": f"Bearer {access_token}"}).url)
print("POST", post("https://leitor-mangas-flutter.firebaseio.com/dados/atualizacao.json",
                   data=f'{{"data": "{str(datetime.now())[:19]}"}}',
                   headers={"Authorization": f"Bearer {access_token}"}).url)
