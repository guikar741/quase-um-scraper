"""."""
from io import StringIO
from json import dump
# from operator import itemgetter
# from os import path, mkdir, remove
from queue import Queue
from re import compile, sub
from threading import Thread
from unicodedata import combining, normalize

from bs4 import BeautifulSoup as bs
# from PIL.Image import open as open_image
from requests import RequestException, get

from constants import *
# import cfscrape
import uuid

# https://unionleitor.top/lista-mangas/a-z/%s/*


def req_link(u, dados={}):
    """."""
    try:
        resul = get(u, data=dados, cookies=cookies)
    except (Exception, RequestException):
        resul = req_link(u, dados)
    return resul


def req_erro_bd(link):
    """."""
    try:
        r = req_link(link)
        if not ('Erro ao conectar ao banco de dados' in r.text):
            return r
        return req_erro_bd(link)
    except Exception:
        return req_erro_bd(link)


def pegar_paginas():
    """."""
    r = req_erro_bd(MANGAS % 1)
    b = bs(r.text, 'html.parser')
    paginacao = b.find("ul", "pagination").find_all('span', "sr-only")
    tam = 0
    for i in paginacao:
        if i.text == 'End':
            tam = int(compile(
                '([0-9]+)').findall(i.parent.get("href"))[0])
    q = Queue()
    [q.put(MANGAS % i) for i in range(1, tam + 1)]
    p = [Thread(target=busca_pag, args=[q]) for i in range(10)]
    [i.start() for i in p]
    [i.join() for i in p]
    junta([i[1] for i in sorted(resQ, key=lambda x: x[0])])


def junta(paginas):
    """."""
    dic = []
    for i in paginas:
        dic.extend(i)
    io = StringIO()
    dump(dic, io)
    json_s = io.getvalue()
    arq = open(path_local + "PaginasMangas.json", 'w')
    arq.write(json_s)
    arq.close()


def busca_pag(fila, direcao=True, maximo=0):
    """."""
    while not fila.empty():
        mangasLink = fila.get()
        print(mangasLink)
        info = []
        r = req_erro_bd(mangasLink)
        b = bs(r.text, 'html.parser')
        mangas = b.find_all('div', 'lista-mangas-novos')

        for i in mangas:
            dic = {'nome': '', 'link': '', 'imagem': ''}
            lin = i.find_all('a')[-1]
            dic['nome'] = lin.text.title()
            # dic['nome'] = lin.text
            dic['link'] = (lin.get("href"))
            dic['imagem'] = (i.find('img').get("src"))
            dic['uuid'] = str(
                uuid.uuid5(uuid.NAMESPACE_DNS, dic['imagem'] + dic['nome'] + dic['link'])
            )
            info.append(dic)
        resQ.append((int(compile('([0-9]+)').findall(mangasLink)[0]), info))


def sanitizestring(palavra):
    """."""
    # Unicode normalize transforma um caracter em seu equivalente em latin.
    nfkd = normalize('NFKD', palavra)
    palavrasemacento = u"".join([c for c in nfkd if not combining(c)])
    # Usa expressão regular para retornar a palavra apenas com números, letras e espaço
    return sub('[^a-zA-Z0-9 ]', '', palavrasemacento)


# scraper = cfscrape.create_scraper()
cookies = None
resQ = []
path_local = 'dados/'
# cookies = dict(__cfduid="d555a366fba45a8cbc112ead394130a721581776002")
# cookies = dict(__cfduid="d555a366fba45a8cbc112ead394130a721581776002")
pegar_paginas()
