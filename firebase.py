"""."""
import firebase_admin
from firebase_admin import firestore
from firebase_admin import credentials
from json import load


def delete_collection(coll_ref, batch_size):
    """."""
    docs = coll_ref.limit(batch_size).get()
    deleted = 0

    for doc in docs:
        print(u'Deleting doc {} => {}'.format(doc.id, doc.to_dict()))
        doc.reference.delete()
        deleted = deleted + 1

    if deleted >= batch_size:
        return delete_collection(coll_ref, batch_size)


def addZeros(i: int, maxNum: int) -> str:
    """."""
    return ''.join(['0' for i in range(len(str(maxNum)) - len(str(i)))]) + str(i)


cred = credentials.Certificate("key.json")
firebase_admin.initialize_app(cred)

db = firestore.client()
batch = db.batch()

mangas = db.collection(u'mangas')
animes = db.collection(u'animes')
hqs = db.collection(u'hqs')

delete_collection(mangas, 100)
delete_collection(animes, 100)
delete_collection(hqs, 100)

mangasList = load(open("dados/PaginasMangas.json"))
print("mangas", len(mangasList))
for i, j in enumerate(mangasList):
    batch.set(mangas.document(addZeros(i + 1, len(mangasList))), j)
    if i % 400 == 0 and i != 0:
        print(i)
        batch.commit()

animesList = load(open("dados/PaginasAnimes.json"))
print("animes", len(animesList))
for i, j in enumerate(animesList):
    batch.set(animes.document(addZeros(i + 1, len(animesList))), j)
    if i % 400 == 0 and i != 0:
        print(i)
        batch.commit()

hqsList = load(open("dados/PaginasHQS.json"))
print("hqs", len(hqsList))
for i, j in enumerate(hqsList):
    batch.set(hqs.document(addZeros(i + 1, len(hqsList))), j)
    if i % 400 == 0 and i != 0:
        print(i)
        batch.commit()
