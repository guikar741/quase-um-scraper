"""."""
from io import StringIO
from json import dump

from bs4 import BeautifulSoup as bs
from requests import RequestException, get
from re import compile
from constants import *
import uuid
# from time import sleep


def req_link(u, dados={}):
    """."""
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'
        }
        resul = get(u, headers=headers, data=dados)
    except (Exception, RequestException):
        resul = req_link(u, dados)
    return resul


def req_hq(indice):
    """."""
    try:
        r = req_link(HQS % indice)
        b = bs(r.text, 'html.parser')
        paginacao = b.find("nav", "blog-pagination")
        paginacao = paginacao.find('a', 'btn-outline-secondary')
        paginacao = paginacao.get('href')
        paginacao = int(compile('([0-9]+)').findall(paginacao)[0])
        return paginacao, b, r
    except Exception:
        return req_hq(indice)


def pegar_paginas():
    """."""
    paginacao, b, r = req_hq(1)
    hqs = []
    print(r.url)
    # executa = True
    while executa:
        paginacao, b = pega_caps(paginacao, b, hqs)
        if paginacao == None and b == None:
            break
    junta(hqs)


def pega_caps(paginacao, b, hqs):
    """."""
    r = None
    try:
        for i in b.find_all('div', 'col-6 col-sm-6 col-md-3'):
            h = {}
            h['nome'] = i.text.strip()
            h['link'] = i.find('a').get('href')
            h['imagem'] = i.find('img').get('src')
            h['uuid'] = str(
                uuid.uuid5(uuid.NAMESPACE_DNS,
                           h['imagem'] + h['nome'] + h['link'])
            )
            hqs.append(h)
        if paginacao == 0:
            return None, None
        paginacao, b, r = req_hq(paginacao)
        print(r.url)
        return paginacao, b
    except Exception:
        print(r.url)
        return pega_caps(paginacao, b, hqs)


def junta(paginas):
    """."""
    dic = paginas
    io = StringIO()
    dump(dic, io)
    json_s = io.getvalue()
    arq = open(path_local + "PaginasHQS.json", 'w')
    arq.write(json_s)
    arq.close()


resQ = []
path_local = 'dados/'
executa = True
pegar_paginas()
